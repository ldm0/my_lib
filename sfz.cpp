int sfz_check(char *sfz)
{
	for(int i = 0; i < 18; ++i)
		sfz[i] -= '0';
	int sum =
		7 * (sfz[0]+sfz[10])
		+9 * (sfz[1]+sfz[11])
		+10 * (sfz[2]+sfz[12])
		+5 * (sfz[3]+sfz[13])
		+8 * (sfz[4]+sfz[14])
		+4 * (sfz[5]+sfz[15])
		+2 * (sfz[6]+sfz[16])
		+1 * sfz[7]
		+6 * sfz[8]
		+3 * sfz[9];
	sum=(12 - (sum % 11)) % 11;
	if((sum == 10 && (sfz[17] == 'x'-'0' || sfz[17] == 'X'-'0')) || (sum==sfz[17]))
		return 1;
	return 0;
}

int main(){
	char sfz[18];
	scanf("%s",sfz);
	if(sfz_check(sfz))
		printf("valid\n");
	else
		printf("invalid\n");
	return 0;
}


