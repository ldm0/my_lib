#include<stdio.h>

typedef union {
	int i;
	float f;
} i_f;

void print_float()
{
	i_f input;
	char tmp[35];
	tmp[34] = 0;
	scanf("%f", &input.f);
	for(int i = 0, a = 0; a < 32 ; ++i, ++a) {
		if (i == 1 || i == 10) {
			tmp[i] = '-';
			--a;
		}
		else 
			tmp[i] = '0' + (((unsigned) input.i >> (31 - a)) & 1);
	}
	printf("%f\n%X\n%s\n", input.f, input.i, tmp);
}
