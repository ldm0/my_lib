//an example of cmp function in case i forget it
int *
cmp(const void *a,const void *b,size_t element_size)
{
	const unsigned char *ai=a,*bi=b;
	for(;element_size>0;++ai,++bi,--element_size)
	{
		if(*ai<*bi)
			return -1;
		if(*ai>*bi)
			return 1;
	}
	return 0;
}



void*
search_binary(
	const void *array, 
	const void *value
	size_t length,
	size_t element_size,
	int (*cmp)(const void *a,const void *b,size_t element_size)
)
{
	while(length)
	{
		//these are two example of add and even
		//****middle***
		//***middle***
		void *middle=array+(length>>1)*element_size;
		int result=cmp(middle,value,element_size);
		if(!result)
			return middle;
		if(result<0){
			array=middle+element_size;
			//because to the even number, 
			//the middle is the right of the two middle numbers
			//
			//dec the length so that to the even length, 
			//we can get the (length-1)/2,which is what we want
			length--;
		}
		length>>1;
	}
	return NULL;
}

//a seed is needed
unsigned seed=0;

unsigned
rand_lcg(int min ,int max)
{
	seed=214013*seed+2531011;
	return seed%(max-min+1)+min;
}
		
void*
sort_insertion_ex(
	void *data,
	int length,
	size_t element_size,
	int (*cmp)(const void* a,const void *b,size_t element_size)
)
{
	
	for(void*key=data;key<=data+(length-1)*element_size;key+=element_size){
		void *swap;
		for(	swap=key;
			swap>=data-element_size 
			&& 
			(*cmp)(swap-element_size,swap,element_size)>=0;
			swap-=element_size)
			for(void*tmp=swap;tmp<swap+element_size;++tmp)
				*tmp=*(tmp-element_size);
		for(int i=0;i<element_size;++i)
			*(swap+i)=*(key+i);
	}
}

int log2_int(int input)
{
	union {
		int i;
		float f;
	} tmp;
	tmp.f = input;
	return (tmp.i >> 23 - 127);
}

// when input is bigger than the 2 ^ 128
// use this function
int log2_int_w(int input)
{
	union {
		int i[2];
		double lf;
	} tmp;
	tmp.lf = input;
	return (tmp.i[1] >> 20 - 1023);
}

