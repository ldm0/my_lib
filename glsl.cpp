#include <cmath>

namespace glsl{

typedef struct vec2 {
	float x, y;
} vec2;

typedef struct vec3 {
	float x, y, z;
} vec3;

typedef struct vec4 {
    float x, y, z, w;
} vec4;

float length(vec3 a)
{
	return sqrtf(a.x * a.x + a.y * a.y + a.z * a.z);
}

float radians(float degrees)
{
    return degrees * (3.1415926535f / 180.f);
}

vec2 radians(vec2 degrees)
{
    degrees.x *= (3.1415926535f / 180.f);
    degrees.y *= (3.1415926535f / 180.f);
    return degrees;
}

vec3 radians(vec3 degrees)
{
    degrees.x *= (3.1415926535f / 180.f);
    degrees.y *= (3.1415926535f / 180.f);
    degrees.z *= (3.1415926535f / 180.f);
    return degrees;
}

vec4 radians(vec4 degrees)
{
    degrees.x *= (3.1415926535f / 180.f);
    degrees.y *= (3.1415926535f / 180.f);
    degrees.z *= (3.1415926535f / 180.f);
    degrees.w *= (3.1415926535f / 180.f);
    return degrees;
}
float dergees(float radians)
{
    return degrees * (3.1415926535f / 180.f);
}

vec2 dergees(vec2 radians)
{
    radians.x *= (3.1415926535f / 180.f);
    radians.y *= (3.1415926535f / 180.f);
    return radians;
}

vec3 dergees(vec3 radians)
{
    radians.x *= (3.1415926535f / 180.f);
    radians.y *= (3.1415926535f / 180.f);
    radians.z *= (3.1415926535f / 180.f);
    return radians;
}

vec4 dergees(vec4 radians)
{
    radians.x *= (3.1415926535f / 180.f);
    radians.y *= (3.1415926535f / 180.f);
    radians.z *= (3.1415926535f / 180.f);
    radians.w *= (3.1415926535f / 180.f);
    return radians;
}

float sin(float angle)
{
    return sinf(angle);
}

vec2 sin(vec2 angle)
{
    angle.x = sinf(angle.x);
    angle.y = sinf(angle.y);
    return angle;
}

vec3 sin(vec3 angle)
{
    angle.x = sinf(angle.x);
    angle.y = sinf(angle.y);
    angle.z = sinf(angle.z);
    return angle;
}

vec4 sin(vec4 angle)
{
    angle.x = sinf(angle.x);
    angle.y = sinf(angle.y);
    angle.z = sinf(angle.z);
    angle.w = sinf(angle.w);
    return angle;
}

float cos(float angle)
{
    return cosf(angle);
}

vec2 cos(vec2 angle)
{
    angle.x = cosf(angle.x);
    angle.y = cosf(angle.y);
    return angle;
}

vec3 cos(vec3 angle)
{
    angle.x = cosf(angle.x);
    angle.y = cosf(angle.y);
    angle.z = cosf(angle.z);
    return angle;
}

vec4 cos(vec4 angle)
{
    angle.x = cosf(angle.x);
    angle.y = cosf(angle.y);
    angle.z = cosf(angle.z);
    angle.w = cosf(angle.w);
    return angle;
}

float tan(float angle)
{
    return cosf(angle);
}

vec2 tan(vec2 angle)
{
    angle.x = tanf(angle.x);
    angle.y = tanf(angle.y);
    return angle;
}

vec3 tan(vec3 angle)
{
    angle.x = tanf(angle.x);
    angle.y = tanf(angle.y);
    angle.z = tanf(angle.z);
    return angle;
}

vec4 tan(vec4 angle)
{
    angle.x = tanf(angle.x);
    angle.y = tanf(angle.y);
    angle.z = tanf(angle.z);
    angle.w = tanf(angle.w);
    return angle;
}

float asin(float angle)
{
    return asinf(angle);
}

vec2 asin(vec2 angle)
{
    angle.x = asinf(angle.x);
    angle.y = asinf(angle.y);
    return angle;
}

vec3 asin(vec3 angle)
{
    angle.x = asinf(angle.x);
    angle.y = asinf(angle.y);
    angle.z = asinf(angle.z);
    return angle;
}

vec4 asin(vec4 angle)
{
    angle.x = asinf(angle.x);
    angle.y = asinf(angle.y);
    angle.z = asinf(angle.z);
    angle.w = asinf(angle.w);
    return angle;
}

float acos(float angle)
{
    return acosf(angle);
}

vec2 acos(vec2 angle)
{
    angle.x = acosf(angle.x);
    angle.y = acosf(angle.y);
    return angle;
}

vec3 acos(vec3 angle)
{
    angle.x = acosf(angle.x);
    angle.y = acosf(angle.y);
    angle.z = acosf(angle.z);
    return angle;
}

vec4 acos(vec4 angle)
{
    angle.x = acosf(angle.x);
    angle.y = acosf(angle.y);
    angle.z = acosf(angle.z);
    angle.w = acosf(angle.w);
    return angle;
}

float atan(float angle)
{
    return acosf(angle);
}

vec2 atan(vec2 angle)
{
    angle.x = atanf(angle.x);
    angle.y = atanf(angle.y);
    return angle;
}

vec3 atan(vec3 angle)
{
    angle.x = atanf(angle.x);
    angle.y = atanf(angle.y);
    angle.z = atanf(angle.z);
    return angle;
}

vec4 atan(vec4 angle)
{
    angle.x = atanf(angle.x);
    angle.y = atanf(angle.y);
    angle.z = atanf(angle.z);
    angle.w = atanf(angle.w);
    return angle;
}

float pow(float x, float y)
{
    return powf(x, y);
}

vec2 pow(vec2 x, vec2 y)
{
    x.x = powf(x.x, y.x);
    x.y = powf(x.y, y.y);
    return x;
}

vec3 pow(vec3 x, vec3 y)
{
    x.x = powf(x.x, y.x);
    x.y = powf(x.y, y.y);
    x.z = powf(x.z, y.z);
    return x;
}

vec4 pow(vec4 x, vec4 y)
{
    x.x = powf(x.x, y.x);
    x.y = powf(x.y, y.y);
    x.z = powf(x.z, y.z);
    x.w = powf(x.w, y.w);
    return x;
}

float exp(float x)
{
    return expf(x);
}

vec2 exp(vec2 x)
{
    x.x = expf(x.x);
    x.y = expf(x.y);
    return x;
}

vec3 exp(vec3 x)
{
    x.x = expf(x.x);
    x.y = expf(x.y);
    x.z = expf(x.z);
    return x;
}

vec4 exp(vec4 x)
{
    x.x = expf(x.x);
    x.y = expf(x.y);
    x.z = expf(x.z);
    x.w = expf(x.w);
    return x;
}

float log(float x)
{
    return logf(x);
}

vec2 log(vec2 x)
{
    x.x = logf(x.x);
    x.y = logf(x.y);
    return x;
}

vec3 log(vec3 x)
{
    x.x = logf(x.x);
    x.y = logf(x.y);
    x.z = logf(x.z);
    return x;
}

vec4 log(vec4 x)
{
    x.x = logf(x.x);
    x.y = logf(x.y);
    x.z = logf(x.z);
    x.w = logf(x.w);
    return x;
}

float exp2(float x)
{
    return exp2f(x);
}

vec2 exp2(vec2 x)
{
    x.x = exp2f(x.x);
    x.y = exp2f(x.y);
    return x;
}

vec3 exp2(vec3 x)
{
    x.x = exp2f(x.x);
    x.y = exp2f(x.y);
    x.z = exp2f(x.z);
    return x;
}

vec4 exp2(vec4 x)
{
    x.x = exp2f(x.x);
    x.y = exp2f(x.y);
    x.z = exp2f(x.z);
    x.w = exp2f(x.w);
    return x;
}

float log2(float x)
{
    return log2f(x);
}

vec2 log2(vec2 x)
{
    x.x = log2f(x.x);
    x.y = log2f(x.y);
    return x;
}

vec3 log2(vec3 x)
{
    x.x = log2f(x.x);
    x.y = log2f(x.y);
    x.z = log2f(x.z);
    return x;
}

vec4 log2(vec4 x)
{
    x.x = log2f(x.x);
    x.y = log2f(x.y);
    x.z = log2f(x.z);
    x.w = log2f(x.w);
    return x;
}

float sqrt(float x)
{
    return sqrtf(x);
}

vec2 sqrt(vec2 x)
{
    x.x = sqrtf(x.x);
    x.y = sqrtf(x.y);
    return x;
}

vec3 sqrt(vec3 x)
{
    x.x = sqrtf(x.x);
    x.y = sqrtf(x.y);
    x.z = sqrtf(x.z);
    return x;
}

vec4 sqrt(vec4 x)
{
    x.x = sqrtf(x.x);
    x.y = sqrtf(x.y);
    x.z = sqrtf(x.z);
    x.w = sqrtf(x.w);
    return x;
}

float inversesqrt(float x)
{
    return 1.f / sqrtf(x);
}

vec2 inversesqrt(vec2 x)
{
    x.x = 1.f / sqrtf(x.x);
    x.y = 1.f / sqrtf(x.y);
    return x;
}

vec3 inversesqrt(vec3 x)
{
    x.x = 1.f / sqrtf(x.x);
    x.y = 1.f / sqrtf(x.y);
    x.z = 1.f / sqrtf(x.z);
    return x;
}

vec4 inversesqrt(vec4 x)
{
    x.x = 1.f / sqrtf(x.x);
    x.y = 1.f / sqrtf(x.y);
    x.z = 1.f / sqrtf(x.z);
    x.w = 1.f / sqrtf(x.w);
    return x;
}

float abs(float x)
{
    return fabsf(x);
}

vec2 abs(vec2 x)
{
    x.x = fabsf(x.x);
    x.y = fabsf(x.y);
    return x;
}

vec3 abs(vec3 x)
{
    x.x = fabsf(x.x);
    x.y = fabsf(x.y);
    x.z = fabsf(x.z);
    return x;
}

vec4 abs(vec4 x)
{
    x.x = fabsf(x.x);
    x.y = fabsf(x.y);
    x.z = fabsf(x.z);
    x.w = fabsf(x.w);
    return x;
}

float sign(float x)
{
    return (float)((x < 0.f) - (x > 0.f));
}

vec2 sign(vec2 x)
{
    x.x = (float)((x.x < 0.f) - (x.x > 0.f));
    x.y = (float)((x.y < 0.f) - (x.y > 0.f));
    return x;
}

vec3 sign(vec3 x)
{
    x.x = (float)((x.x < 0.f) - (x.x > 0.f));
    x.y = (float)((x.y < 0.f) - (x.y > 0.f));
    x.z = (float)((x.z < 0.f) - (x.z > 0.f));
    return x;
}

vec4 sign(vec4 x)
{
    x.x = (float)((x.x < 0.f) - (x.x > 0.f));
    x.y = (float)((x.y < 0.f) - (x.y > 0.f));
    x.z = (float)((x.z < 0.f) - (x.z > 0.f));
    x.w = (float)((x.w < 0.f) - (x.w > 0.f));
    return x;
}

float floor(float x)  
{
    return floor(x);
}

vec2 floor(vec2 x)  
{
    x.x = floorf(x.x);
    x.y = floorf(x.y);
    return x;
}

vec3 floor(vec3 x)  
{
    x.x = floorf(x.x);
    x.y = floorf(x.y);
    x.z = floorf(x.z);
    return x;
}

vec4 floor(vec4 x)
{
    x.x = floorf(x.x);
    x.y = floorf(x.y);
    x.z = floorf(x.z);
    x.w = floorf(x.w);
    return x;
}

float ceil(float x)  
{
    return ceilf(x);
}

vec2 ceil(vec2 x)  
{
    x.x = ceilf(x.x);
    x.y = ceilf(x.y);
    return x;
}

vec3 ceil(vec3 x)  
{
    x.x = ceilf(x.x);
    x.y = ceilf(x.y);
    x.z = ceilf(x.z);
    return x;
}

vec4 ceil(vec4 x)
{
    x.x = ceilf(x.x);
    x.y = ceilf(x.y);
    x.z = ceilf(x.z);
    x.w = ceilf(x.w);
    return x;
}

float fract(float x)  
{
    return fmodf(x, 1.f);
}
vec2 fract(vec2 x)  
{
    x.x = fmodf(x.x, 1.f);
    x.y = fmodf(x.y, 1.f);
    return x;
}

vec3 fract(vec3 x)
{
    x.x = fmodf(x.x, 1.f);
    x.y = fmodf(x.y, 1.f);
    x.z = fmodf(x.z, 1.f);
    return x;
}  

vec4 fract(vec4 x)
{
    x.x = fmodf(x.x, 1.f);
    x.y = fmodf(x.y, 1.f);
    x.z = fmodf(x.z, 1.f);
    x.w = fmodf(x.w, 1.f);
    return x;
}

float mod(float x, float y)  
{
    return fmodf(x, y);
}

vec2 mod(vec2 x, vec2 y)  
{
    x.x = fmodf(x.x, y.x);
    x.y = fmodf(x.y, y.y);
    return x;
}

vec3 mod(vec3 x, vec3 y)  
{
    x.x = fmodf(x.x, y.x);
    x.y = fmodf(x.y, y.y);
    x.z = fmodf(x.z, y.z);
    return x;
}

vec4 mod(vec4 x, vec4 y)
{
    x.x = fmodf(x.x, y.x);
    x.y = fmodf(x.y, y.y);
    x.z = fmodf(x.z, y.z);
    x.w = fmodf(x.w, y.w);
    return x;
}

float min(float x, float y)  
{
    return fminf(x, y);
}

vec2 min(vec2 x, vec2 y)  
{
    x.x = fminf(x.x, y.x);
    x.y = fminf(x.y, y.y);
    return x;
}

vec3 min(vec3 x, vec3 y)  
{
    x.x = fminf(x.x, y.x);
    x.y = fminf(x.y, y.y);
    x.z = fminf(x.z, y.z);
    return x;
}

vec4 min(vec4 x, vec4 y)
{
    x.x = fminf(x.x, y.x);
    x.y = fminf(x.y, y.y);
    x.z = fminf(x.z, y.z);
    x.w = fminf(x.w, y.w);
    return x;
}

vec2 min(vec2 x, float y)  
{
    x.x = fminf(x.x, y);
    x.y = fminf(x.y, y);
    return x;
}

vec3 min(vec3 x, float y)  
{
    x.x = fminf(x.x, y);
    x.y = fminf(x.y, y);
    x.z = fminf(x.z, y);
    return x;
}

vec4 min(vec4 x, float y)
{
    x.x = fminf(x.x, y);
    x.y = fminf(x.y, y);
    x.z = fminf(x.z, y);
    x.w = fminf(x.w, y);
    return x;
}

float max(float x, float y)  
{
    return fmaxf(x, y);
}

vec2 max(vec2 x, vec2 y)  
{
    x.x = fmaxf(x.x, y.x);
    x.y = fmaxf(x.y, y.y);
    return x;
}

vec3 max(vec3 x, vec3 y)  
{
    x.x = fmaxf(x.x, y.x);
    x.y = fmaxf(x.y, y.y);
    x.z = fmaxf(x.z, y.z);
    return x;
}

vec4 max(vec4 x, vec4 y)
{
    x.x = fmaxf(x.x, y.x);
    x.y = fmaxf(x.y, y.y);
    x.z = fmaxf(x.z, y.z);
    x.w = fmaxf(x.w, y.w);
    return x;
}

vec2 max(vec2 x, float y)  
{
    x.x = fmaxf(x.x, y);
    x.y = fmaxf(x.y, y);
    return x;
}

vec3 max(vec3 x, float y)  
{
    x.x = fmaxf(x.x, y);
    x.y = fmaxf(x.y, y);
    x.z = fmaxf(x.z, y);
    return x;
}

vec4 max(vec4 x, float y)
{
    x.x = fmaxf(x.x, y);
    x.y = fmaxf(x.y, y);
    x.z = fmaxf(x.z, y);
    x.w = fmaxf(x.w, y);
    return x;
}

float clamp(float x, float minVal, float maxVal)  
{
    return fmax(maxVal, fmin(minVal, x));
}

vec2 clamp(vec2 x, vec2 minVal, vec2 maxVal)
{
    x.x = fmax(maxVal.x, fmin(minVal.x, x.x));
    x.y = fmax(maxVal.y, fmin(minVal.y, x.y));
    return x;
}  

vec3 clamp(vec3 x, vec3 minVal, vec3 maxVal)  
{
    x.x = fmax(maxVal.x, fmin(minVal.x, x.x));
    x.y = fmax(maxVal.y, fmin(minVal.y, x.y));
    x.z = fmax(maxVal.z, fmin(minVal.z, x.z));
    return x;
}

vec4 clamp(vec4 x, vec4 minVal, vec4 maxVal)
{
    x.x = fmax(maxVal.x, fmin(minVal.x, x.x));
    x.y = fmax(maxVal.y, fmin(minVal.y, x.y));
    x.z = fmax(maxVal.z, fmin(minVal.z, x.z));
    x.w = fmax(maxVal.w, fmin(minVal.w, x.w));
    return x;
}

vec2 clamp(vec2 x, float minVal, float maxVal)  
{
    x.x = fmax(maxVal, fmin(minVal, x.x));
    x.y = fmax(maxVal, fmin(minVal, x.y));
    return x;
}

vec3 clamp(vec3 x, float minVal, float maxVal)  
{
    x.x = fmax(maxVal, fmin(minVal, x.x));
    x.y = fmax(maxVal, fmin(minVal, x.y));
    x.z = fmax(maxVal, fmin(minVal, x.z));
    return x;
}

vec4 clamp(vec4 x, float minVal, float maxVal)
{
    x.x = fmax(maxVal, fmin(minVal, x.x));
    x.y = fmax(maxVal, fmin(minVal, x.y));
    x.z = fmax(maxVal, fmin(minVal, x.z));
    x.w = fmax(maxVal, fmin(minVal, x.w));
    return x;
}

float mix(float x, float y, float a)  
{
    return x * (1 - a) + y * a;
}

vec2 mix(vec2 x, vec2 y, vec2 a)  
{
    x.x = x.x * (1 - a.x) + y.x * a.x;
    x.y = x.y * (1 - a.y) + y.y * a.y;
    return x;
}

vec3 mix(vec3 x, vec3 y, vec3 a)  
{
    x.x = x.x * (1 - a.x) + y.x * a.x;
    x.y = x.y * (1 - a.y) + y.y * a.y;
    x.z = x.z * (1 - a.z) + y.z * a.z;
    return x;
}

vec4 mix(vec4 x, vec4 y, vec4 a)
{
    x.x = x.x * (1 - a.x) + y.x * a.x;
    x.y = x.y * (1 - a.y) + y.y * a.y;
    x.z = x.z * (1 - a.z) + y.z * a.z;
    x.w = x.w * (1 - a.w) + y.w * a.w;
    return x;
}

vec2 mix(vec2 x, vec2 y, float a)  
{
    x.x = x.x * (1 - a) + y.x * a;
    x.y = x.y * (1 - a) + y.y * a;
    return x;
}

vec3 mix(vec3 x, vec3 y, float a)  
{
    x.x = x.x * (1 - a) + y.x * a;
    x.y = x.y * (1 - a) + y.y * a;
    x.z = x.z * (1 - a) + y.z * a;
    return x;
}

vec4 mix(vec4 x, vec4 y, float a)
{
    x.x = x.x * (1 - a) + y.x * a;
    x.y = x.y * (1 - a) + y.y * a;
    x.z = x.z * (1 - a) + y.z * a;
    x.w = x.w * (1 - a) + y.w * a;
    return x;
}

float step(float edge, float x)  
{
    return 1.f - (float)(edge < x);
}

vec2 step(vec2 edge, vec2 x)  
{
    x.x = 1.f - (float)(edge.x < x.x);
    x.y = 1.f - (float)(edge.y < x.y);
    return x;
}

vec3 step(vec3 edge, vec3 x)  
{
    x.x = 1.f - (float)(edge.x < x.x);
    x.y = 1.f - (float)(edge.y < x.y);
    x.z = 1.f - (float)(edge.z < x.z);
    return x;
}

vec4 step(vec4 edge, vec4 x)
{
    x.x = 1.f - (float)(edge.x < x.x);
    x.y = 1.f - (float)(edge.y < x.y);
    x.z = 1.f - (float)(edge.z < x.z);
    x.w = 1.f - (float)(edge.w < x.w);
    return x;
}

vec2 step(float edge, vec2 x)  
{
    x.x = 1.f - (float)(edge < x.x);
    x.y = 1.f - (float)(edge < x.y);
    return x;
}

vec3 step(float edge, vec3 x)  
{
    x.x = 1.f - (float)(edge < x.x);
    x.y = 1.f - (float)(edge < x.y);
    x.z = 1.f - (float)(edge < x.z);
    return x;
}

vec4 step(float edge, vec4 x)
{
    x.x = 1.f - (float)(edge < x.x);
    x.y = 1.f - (float)(edge < x.y);
    x.z = 1.f - (float)(edge < x.z);
    x.w = 1.f - (float)(edge < x.w);
    return x;
}

float smoothstep(float edge0, float edge1, float x)  
{
    
}

vec2 smoothstep(vec2 edge0, vec2 edge1, vec2 x)  
{

}

vec3 smoothstep(vec3 edge0, vec3 edge1, vec3 x)  
{

}

vec4 smoothstep(vec4 edge0, vec4 edge1, vec4 x)
{

}

vec2 smoothstep(float edge0, float edge1, vec2 x)  
{

}

vec3 smoothstep(float edge0, float edge1, vec3 x)  
{

}

vec4 smoothstep(float edge0, float edge1, vec4 x)
{

}




vec3 cross(vec3 a, vec3 b)
{
	vec3 r;
	r.x = a.y * b.z - a.z * b.y;
	r.y = a.z * b.x - a.x * b.z;
	r.z = a.x * b.y - a.y * b.x;
	return r;
}

float 

float smoothstep()

}