#pragma once
#include<Windows.h>

struct State {
	bool w;
	bool a;
	bool s;
	bool d;
	bool mouse_down;
	POINT mouse_position;
};

//Assume the HID remains from start to end
//Singleton
class HID {

	HID *instance;

	HANDLE main_thread_handle; //currently not be used
	DWORD main_thread_id;

	State state;

	static char communicate_ch;
	static DWORD WINAPI main_thread(LPVOID input);

public:
	HID();
	~HID();

	static HID& get();

	bool is_wdown(void) { return state.w; }
	bool is_adown(void) { return state.a; }
	bool is_sdown(void) { return state.s; }
	bool is_ddown(void) { return state.d; }
	POINT get_mouse_position(void) { return state.mouse_position; }
};

