namespace my_lib {
	class Stack {
	private:
		static const int s_capacity_default = 256;
		int m_top;
		int m_capacity;
		char *m_data;
	public:
		Stack(int capacity) {
			if (capacity >= 1) {
				m_capacity = capacity;
			} else {
				m_capacity = s_capacity_default;
			}
			m_data = (char *)operator new(m_capacity * sizeof(char));
			m_top = 0;
		}

		~Stack()
		{
			delete(m_data);
		}

		bool push(char input) {
			char *tmp_m_data;
			if (m_top < 0)
				return false;
			if (m_top >= m_capacity) {
				tmp_m_data = (char *)operator new(m_capacity * 2 * sizeof(char)); 
				if (tmp_m_data == nullptr || m_data == nullptr)
					return false;
				m_capacity *= 2;
				memcpy(tmp_m_data, m_data, sizeof(char) * m_top);
				delete(m_data);
				m_data = tmp_m_data;
			}
			m_data[m_top++] = input;
			return true;
		}

		char pop(void) {
			return m_data[--m_top];
		}

		bool full(void)
		{
			return m_top >= m_capacity;
		}

		bool empty(void)
		{
			return m_top == 0;
		}

		void clear(void)
		{
			m_top = 0;
		}

		bool shrink(void)
		{
			union {
				int i[2];
				double lf;
			} tmp;
			tmp.lf = m_top;
			int tmp_capacity = 1 << ((tmp.i[1] >> 20) - 1023);
			char * tmp_m_data = (char *)operator new(tmp_capacity * sizeof(char));
			if (!tmp_m_data)
				return false;
			memcpy(tmp_m_data, m_data, m_top * sizeof(char));
			m_data = tmp_m_data;
			m_capacity = tmp_capacity;
		}
	};
}
