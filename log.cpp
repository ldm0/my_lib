#include<stdio.h>
namespace my_lib {
	class Log {
	public:
		void log_int(int input) { printf("%d\n", input); }
		void log_unsigned(unsigned input) { printf("%u\n", input); }
		void log_char(char input) { printf("%c\n", input); }
		void log_float(float input) { printf("%f\n", input); }
		void log_double(double input) { printf("%lf\n", input); }
		void log_long_double(long double input) { printf("%Lf\n", input); }
		template <typename T> void log_hex(T input)
		{
			int num_input_cp = sizeof(input) / sizeof(char);
			char *input_cp = (char *)operator new(num_input_cp * sizeof(char));
			memcpy(input_cp, &input, num_input_cp * sizeof(char));
			for (int i = num_input_cp - 1; i >= 0; --i)
				printf("%hhx", input_cp[i]);
			putchar('\n');
		}
		void log_double_b(double input)
		{
			union {
				long long ld;
				double lf;
			} ld_lf;
			char tmp[67];
			tmp[66] = '\0';
			ld_lf.lf = input;
			for(int i = 0, a = 0; a < 32 ; ++i, ++a) {
				if (i == 1 || i == 13) {
					tmp[i] = '-';
					--a;
				}
				else 
					tmp[i] = '0' + (((unsigned long long) ld_lf.ld >> (63 - a)) & 1);
			}
			printf("%s\n", tmp);
		}

		void log_float_b(float input)
		{
			union {
				int i;
				float f;
			} i_f;
			char tmp[35];
			tmp[34] = 0;
			i_f.f = input;
			for(int i = 0, a = 0; a < 32 ; ++i, ++a) {
				if (i == 1 || i == 10) {
					tmp[i] = '-';
					--a;
				}
				else 
					tmp[i] = '0' + (((unsigned) i_f.i >> (31 - a)) & 1);
			}
			printf("%s\n", tmp);
		}
	};
}
