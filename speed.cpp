//ment to test speed and visualize the time spent;
#include<stdio.h>
#include<unistd.h>
#include<time.h>

#include"log.cpp"

using namespace my_lib;

void foo_1() { }
void foo_2() { }

int main()
{
	//do sth
	clock_t start, end;
	long double time[2];
	start = clock();
	foo_1();
	end= clock();
	time[0] = (long double)(end - start);
	
	start = clock();
	foo_2();
	end= clock();
	time[1] = (long double)(end - start);

	Log::log_long_double(time[0]);
	Log::log_long_double(time[1]);
	//do sth
}
