typedef union{
    int i;
    float f;
} int_or_float;

vec3 hsv2rgb(vec3 hsv) 
{
    vec3 rgb;
    int_or_float _tmp.i = (23 + 127) << 23;
    int_or_float step.f = h.x * 6 + _tmp.f;
    step.i -= _tmp.i;
    float ext = h.x * 6 - (float)step.i;
    switch(step.i)
    {
    case 0:
        rgb.x = 1.;
        rgb.y = ext;
        rgb.z = 0.;
        break;
    case 1:
        rgb.x = 1. - ext;
        rgb.y = 1.;
        rgb.z = 0.;
        break;
    case 2:
        rgb.x = 0.;
        rgb.y = 1.;
        rgb.z = ext;
        break;
    case 3:
        rgb.x = 0.;
        rgb.y = 1. - ext;
        rgb.z = 1.;
        break;
    case 4:
        rgb.x = ext;
        rgb.y = 0.;
        rgb.z = 1.;
        break;
    case 5:
        rgb.x = 1.;
        rgb.y = 0.;
        rgb.z = 1 - ext;
        break;
    }
    float _sv = hsv.y * hsv.z;
    rgb *= _sv;
    rgb += v - _sv;
    return rgb;
}

vec3 rgb2hsv(vec3 rgb)
{
    vec3 hsv;
    int max, min;
    float a_rgb[3] = {rgb.x, rgb.y, rgb.z};
    max_min(a_rgb, &max, &min);
    hsv.z = a_rgb[max];
    float _gap = a_rgb[max] - a_rgb[min];
    if (gap == 0) {
        hsv.y = 0;
        hsv.x = 0;
        return hsv;
    }
    hsv.y = _gap / hsv.z;
    return hsv;
}