#include<stdio.h>

typedef union {
	long long ld;
	double lf;
} ld_lf;

void print_double()
{
	ld_lf input;
	char tmp[67];
	tmp[66] = '\0';
	scanf("%lf", &input.lf);
	for(int i = 0, a = 0; a < 32 ; ++i, ++a) {
		if (i == 1 || i == 13) {
			tmp[i] = '-';
			--a;
		}
		else 
			tmp[i] = '0' + (((unsigned long long) input.ld >> (63 - a)) & 1);
	}
	printf("%lf\n%llX\n%s\n", input.lf, input.ld, tmp);
}
