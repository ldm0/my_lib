//for linux
#include<ncurses.h>
#include<unistd.h>
#include<math.h>
#include<time.h>


struct vec2 {
	float x, y;
};

struct vec3 {
	float x, y, z;
};

struct Ray {
	struct vec3 o, d;
};

float length(struct vec3 a)
{
	return sqrtf(a.x * a.x + a.y * a.y + a.z * a.z);
}

struct vec3 cross(struct vec3 a, struct vec3 b)
{
	struct vec3 r;
	r.x = a.y * b.z - a.z * b.y;
	r.y = a.z * b.x - a.x * b.z;
	r.z = a.x * b.y - a.y * b.x;
	return r;
}

//Gray val
float ball(struct vec2 coord, float time)
{
	struct vec2 uv;
	uv.x = coord.x;
	uv.y = coord.y;

	struct Ray r = {
		{0.f, 0.f, -10.f},
		{uv.x, uv.y, 0.f}
	};
	struct vec3 p = {cos(time/20), 0., 50. + 25 * sin(time/20)};
	struct vec3 tmp1;
	tmp1.x = r.d.x - r.o.x;
	tmp1.y = r.d.y - r.o.y;
	tmp1.z = r.d.z - r.o.z;
	struct vec3 tmp2;
	tmp2.x = p.x - r.o.x;
	tmp2.y = p.y - r.o.y;
	tmp2.z = p.z - r.o.z;
	return length(cross(tmp1, tmp2)) / length(tmp1);
}

int main()
{
	initscr();
	struct vec2 uv;
	clock_t time;
	for(;;) {
		time = clock();
		for(int y = 0; y < LINES; ++y) {
			for (int x = 0; x < COLS; ++x) {
				uv.x = (float)x / (float)COLS- .5;
				uv.y = (float)y / (float)LINES - .5;
				mvaddch(y, x, ball(uv,time / 1000.) > .9 ? ' ' : '6');
			}
		}
		refresh();
		usleep(50000);
	}
	getch();
	endwin();
	return 0;
}

