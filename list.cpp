namespace my_lib {

	struct list_node {
		char *m_data;
		list_node *m_next;
	}; // COW

	class List {
		size_t m_val_size;
		list_node *m_head;
		list_node *m_tail;
	public:
		List(size_t val_size): m_val_size(val_size) {
			m_head = operator new(sizeof(list_node));
			m_tail = m_head;
		};

		~list() {
			if (m_head == nullptr)
				return;
			list_node *tmp = m_head->m_next;	
			for (;;) {
				if (m_head->m_data != nullptr)
					delete(m_head->m_data);
				delete(m_head);
				if (tmp == nullptr)
					break;
				m_head = tmp;
				tmp = m_head->m_next;
			}
		};

		bool push_back(list_node *input) {
			if (input == nullptr)
				return false;
			m_tail.m_next= input;
			m_tail = m_tail->m_next;
			return true;
		};

		bool pop_front(list_node *output) {
			if (output == null)
				return false;
			memcpy(output, m_head, sizeof(list_node));
			list_node *tmp = m_head;
			m_head = m_head->next;
			delete(m_head);
		};
	};
}
