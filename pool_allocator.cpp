namespace my_lib {

	class chunk_node {
	public:
		char *m_data;
		chunk_node *m_next;

		chunk_node(void *data, chunk_node *next = nullptr)
			: m_data((char *)data), m_next(next)
	       	{
	       	};

		~chunk_node() 
		{
		};
	};

	class chunk_list {
		chunk_node *m_head;
		chunk_node *m_tail;
	public:
		chunk_list()
		{
			m_head = new chunk_node(nullptr, nullptr);
			m_tail = m_head;
		};
		~chunk_list()
		{
			chunk_node *tmp;
			while (m_head != m_tail) {
				tmp = m_head->m_next;
				delete(m_head);
				m_head = tmp;
			}
		};
		//bool push_front(chunk_node &input)
		//{
			//chunk_node *tmp = new chunk_node(input.m_data, m_head);
			//if (tmp == nullptr)
				//return false;
			//m_head = tmp;
		//};
		//the input is supposed to be in stack rather in heap
		//because we copy the data
		bool put(chunk_node *input) //push_back
		{
			if (input == nullptr)
				return false;
			chunk_node *tmp = new chunk_node(input->m_data, nullptr);
			if (tmp == nullptr)
				return false;
			m_tail->m_next = tmp;
			m_tail = tmp;
			return false;
		};

		bool get(chunk_node **out)
		{
			if (m_head == m_tail)
				return false;
			if (*out == nullptr)
				return false;
			*out = m_head->m_next;
			m_head->m_next = (*out)->m_next;
			return true;
		}
	};

	class Pool_allocator {
		size_t m_chunk_size;	//sizeof each chunk of data
		int m_num_chunks;
		chunk_list m_chunk_list;	//available chunks
		char *m_data;
	public:
		Pool_allocator(int num_chunk, size_t chunk_size)
		{
			m_data = (char *)operator new(m_num_chunks * chunk_size);
			if (m_data == nullptr)
				return;
			chunk_node *tmp;
			for (int i = 0; i < num_chunk; ++i) {
				tmp->m_data = m_data + i * m_num_chunks;
				m_chunk_list.put(tmp);
			}
		}
		~Pool_allocator()
		{
			delete(m_data);
		}

		bool get(void **data)
		{
			chunk_node *tmp;
			if (m_chunk_list.get(&tmp) == false)
				return false;
			*data = tmp->m_data;
			delete(tmp);
		}

		bool put(void *data)
		{
			chunk_node tmp(data);
			m_chunk_list.put(&tmp);
		}
	};
}
