#include<Windows.h>
#include<stdio.h>
#include<math.h>

#define ALERT(a) printf(#a"\n")

int main()
{
	INPUT_RECORD input_record;
	DWORD read_length;
	DWORD unread_events;
	for(;;){
		ReadConsoleInput(GetStdHandle(STD_INPUT_HANDLE),&input_record,1,&read_length);
		if (input_record.EventType == KEY_EVENT){
			printf("%d\t%d\n",input_record.Event.KeyEvent.uChar.AsciiChar,input_record.Event.KeyEvent.dwControlKeyState);
			ALERT(KEY_EVENT);
		}
		else if (input_record.EventType == MOUSE_EVENT)
			ALERT(MOUSE_EVENT);
		else if (input_record.EventType == MENU_EVENT)
			ALERT(MENU_EVENT);
		else if (input_record.EventType == FOCUS_EVENT)
			ALERT(FOCUS_EVENT);
		else if(input_record.EventType == WINDOW_BUFFER_SIZE_EVENT)
			ALERT(WINDOW_BUFFER_SIZE_EVENT);
	}
}