#include<iostream>

namespace my_lib {
	class Singleton{
	private:
		Singleton() {};
	public:
		static Singleton& get()
		{
			static Singleton instance;
			return instance; 
		};
	};
}