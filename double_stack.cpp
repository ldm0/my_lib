namespace my_lib {
    class Double_stack {
        size_t m_top1;
        size_t m_top2;
        size_t m_capacity;
        void *m_data;
    public:
        Double_stack(size_t capacity = 1024000) 
            :m_capacity(capacity)
        {
            m_top1 = 0;
            m_top2 = capacity - 1;
            m_data = (void *)operator new(capacity);
        }

        ~Double_stack() {
            delete(m_data);
        }

        bool check_full(size_t size_inc)
        {
            return m_top1 + size_inc > m_top2;
        }
        bool push_low(void *data, size_t size) {
            if (data == nullptr || (size == (size_t)0))
                return false;
            if (check_full(size + sizeof(size_t)))
                return false;
            memcpy(m_data + m_top1, data, size);
            memcpy(m_data + m_top1 + size, &size, sizeof(size_t));
            m_top1 += size + sizeof(size_t);
            return true;
        }
        bool push_high(void *data, size_t size) {
            if (data == nullptr || (size == (size_t)0))
                return false;
            if (check_full(size + sizeof(size_t)))
                return false;
            memcpy(m_data + m_top2 - size, data, size);
            memcpy(m_data + m_top2 - size - sizeof(size_t), &size, sizeof(size_t));
            m_top2 -= size + sizeof(size_t);
            return true;
        }
        bool pop_low(void *data, size_t *size) {
            if (size == nullptr || data == nullptr)
                return false;
            if (m_top1 == 0)
                return false;
            memcpy(size, m_data + m_top1 - 1, sizeof(size_t));
            memcpy(data, m_data + m_top1 - 1 - sizeof(size_t), *size);
            m_top1 -= *size + sizeof(size_t);
            return true;
        }
        bool pop_high(void *data, size_t *size) {
            if (size == nullptr || data == nullptr)
                return false;
            memcpy(size, m_data + m_top2 + 1, sizeof(size_t));
            memcpy(data, m_data + m_top2 + 1 + sizeof(size_t), *size);
            m_top2 += *size + sizeof(size_t);
            return true;
        }
    };
}