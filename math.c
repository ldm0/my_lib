#include"math.h"

#pragma region root



#pragma endregion

#pragma region rootMagic

#pragma region rootMagic_double

//***********************************************
//
//double magic 0x3FEF478B2FC6BBFA
//
//***********************************************

//3/2(magic)
//quick_reciprocal square root
double 
Q_rsqrt(double a) {
	double ahf = 0.5*a;
	unsigned long long i = *(unsigned long long*)&a;
	i = 0x5FE6EB50C7AA19F9 - (i >> 1);
	a = *(double*)&i;
	return a * (1.5 - ahf * a*a);
}

//1/2(magic)
//quick_square root
double 
Q_sqrt(double a) {
	double abak = a;
	unsigned long long i = *(unsigned long long*)&a;
	i = 0x1FF7A3C597E35DFD + (i >> 1);
	a = *(double*)&i;
	return (a*a + abak) / (2 * a);
}

//2/3(magic)
//quick_cube root
double 
Q_crt(double a) {
	double abak = a;
	unsigned long long i = *(unsigned long long*)&a;
	i = 0x2A9F8590D8D4877C + (i*(1 / 3));
	a = *(double*)&i;
	double sqra = a * a;
	return (2 * a*sqra + abak) / (3 * sqra);
}

//2(magic)
//quick_inverse
double 
Q_inverse(double a) {
	double abak = a;
	unsigned long long i = *(unsigned long long*)&a;
	i = 0x7FDE90B28A7D9678 - i;
	a = *(double*)&i;
	return a * (2 - abak * a);
}

#pragma endregion

#pragma region rootMgic_float

//***********************************************
//
//float magic 0x3F7A3BEA
//
//***********************************************

//3/2(magic)
//quick_reciprocal square root
float 
Q_rsqrt(float a) {
	float ahf = 0.5*a;
	unsigned i = *(unsigned&)&a;
	i = 0x5F3759DF - (i >> 1);
	a = *(float*)&i;
	return a * (1.5 - ahf * a*a);
}

//1/2(magic)
//quick_square root
float 
Q_sqrt(float a) {
	float abak = a;
	unsigned i = *(unsigned*)&a;
	i = 0x1FBD1DF5 + (i >> 1);
	a = *(float*)&i;
	return (a*a + abak) / (2 * a);
}

//2/3(magic)
//quick_cube root
float 
Q_crt(float a) {
	float abak = a;
	unsigned i = *(unsigned*)&a;
	i = 0x2A517D46 + (i / 3);
	a = *(float*)&i;
	float sqra = a * a;
	return (2 * a*sqra + abak) / (3 * sqra);
}

//2(magic)
//quick_inverse
float 
Q_rcpc(float a) {
	float abak = a;
	unsigned i = *(unsigned*)&a;
	i = 0x7EF477D4 - i;
	a = *(float*)&i;
	return a * (2 - abak * a);
}

#pragma endregion

#pragma endregion

#pragma region GCD

//i dont know the loop version whether faster than tail recursion version :)
//may be gcc has tail recursion optimization :)
int 
gcd(int a,int b){
	int tmp;
	while(a){
		tmp=a;
		a=b%a;
		b=tmp;
	}
	return b;
}

//tail recursion version
int
gcd_recursion(int a,int b){
	if(!a)	return b;
	gcd(b%a,a);
}

//we get x and y of the equation: ax+by=gcd(a,b);
//name :gcd_extended
int
gcd(int a,int b,int *x,int *y){
	if(!b){
		*x=1;
		*y=0;
		return a;
	}
	int result=gcd(a,b,y,x);
	*y-=(a/b)*(*x);
	return result;
}

#pragma endregion

typedef union {
	int i;
	float f;
} int_or_float;

inline int floor(float f)
{
	// i < 2^23;
	int_or_float _tmp;
	int_or_float _i;
	_tmp.i = (23 + 127) << 23;
	_i.f = f;
	_i.f += _tmp.f;
	_i.i -= _tmp.i;
	return _i.i;
}

inline int round(float f)
{
	// i < 2^22
	int_or_float _tmp;
	int_or_float _i;
	_tmp.i = ((23 + 127) << 23) + 1 << 22;
	_i.f = f;
	_i.f += _tmp.f;
	_i.i -= _tmp.i;
	return _i.i;
}

//the speed is the same as the average, but this is way geeker;
inline float saturate(float i)
{
	int_or_float _result;
	int _tmp;
	_result.f = i;
	_tmp = _result.i >> 31;
	_tmp = ~_tmp;
	_result.i &= _tmp;
	_result.f -= 1.f;
	_tmp = _result.i >> 31;
	_result.i &= _tmp;
	_result.f += 1.f;
	return _result.f;
}


inline void max_min (int *a, int *max, int *min)
{
	int b[3] = {a[0] < a[1], a[1] < a[2], a[2] < a[0]};
	if (b[0]) {
		if (b[2]) {
			min = 2;
			max = 1;
		} else {
			min = 0;
			if (b[1]) {
				max = 2;
			} else {
				max = 1;
			}
		}
	} else {
		if (b[2]) {
			max = 0;
			if (b[1]) {
				min = 1;
			} else {
				min = 2;
			}
		} else {
			min = 1;
			max = 2;
		}
	}
}
