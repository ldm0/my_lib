#include"HID.h"

#include<assert.h>

//C style function, so use ExitThread is just fine
//And can also reduce an "if" branch;
DWORD WINAPI HID::main_thread(LPVOID input)
{
	static const int interval = 15;

	State *state = (State *)input;

	for (;;) {
		//Get the mouse position
		GetCursorPos(&(state->mouse_position));

		//check if specific keys down
		Sleep(interval);
	}
	return 0;
}

HID::HID()
{
	state.w = false;
	state.a = false;
	state.s = false;
	state.d = false;
	state.mouse_down = false;
	GetCursorPos(&state.mouse_position);

	//create a thread to detect the input
	main_thread_handle = CreateThread(NULL, 0, main_thread, &state, 0, &main_thread_id);
}

HID::~HID()
{
	//wait for the thread to terminate
	//for no more than 1s
	if (main_thread_handle)
		assert(TerminateThread(main_thread_handle, 0));
}

HID& HID::get()
{
	static HID instance;
	return instance;
}

