#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<string.h>
#include<limits.h>

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MAP_SIZE 10000
#define RAND_NUM 100000000
#define TEST_NUM 100

uint32_t lcg(uint32_t *seed) {
	*seed = 214013 * (*seed) + 2531011;
	return (*seed) % MAP_SIZE;
}

int main()
{
        uint32_t seed = 11111111;
        volatile int i = 0;
	uint32_t sum = 0;
        for(; i < TEST_NUM; ++i) {
                uint16_t *map = (uint16_t *)malloc(MAP_SIZE * sizeof(uint16_t));
                memset(map, 0, MAP_SIZE *sizeof(uint16_t));
                if (!map) {
                        fprintf(stderr, "cannot malloc the map!\n");
                        return -1;
                }
                volatile int j = 0;
                uint16_t map_max = 0;
                uint32_t map_max_i = -1;
                for (; j < RAND_NUM; ++j) {
			uint32_t num_1 = lcg(&seed);
			uint32_t num_2 = lcg(&seed);
                        uint32_t tmp_index = MAX(num_1, num_2);
                        uint16_t tmp_map_val = ++(map[tmp_index]);
                        if (tmp_map_val > map_max) {
                                map_max = tmp_map_val;
                                map_max_i = tmp_index;
                        }
                }
                free(map);
                printf("rand %d times, max hit times: %d, index : %d, of %d\n",RAND_NUM, map_max, map_max_i, MAP_SIZE);
		sum += map_max_i;
        }
	printf("%d\n", sum / TEST_NUM);
        return 0;
}
